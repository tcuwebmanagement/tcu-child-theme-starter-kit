<?php
/**
 * Child Theme Started Kit functions and definitions.
 *
 * Set up the theme and provides some function helpers.
 * Make sure you update this paragraph and provide a better summary.
 *
 * @package WordPress
 * @since Child Theme Started Kit 1.0.0
 */

/**
 *
 * Enqueue parent style sheet
 * as well as the child style sheet
 *
 * Note: Please change the function name
 */
function my_child_theme_enqueue_styles() {

	$child_style = 'child-style';

	wp_enqueue_style( $child_style,
		/* Edit the path to your child themes stylesheet */
		get_stylesheet_directory_uri() . '/library/css/style.css',
		array( 'tcu-stylesheet' )
	);
}

add_action( 'wp_enqueue_scripts', 'my_child_theme_enqueue_styles' );
