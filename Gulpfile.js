// Require our dependencies
const autoprefixer = require( 'autoprefixer' );
const babel = require( 'gulp-babel' );
const browserSync = require( 'browser-sync' );
const concat = require( 'gulp-concat' );
const cssnano = require( 'gulp-cssnano' );
const del = require( 'del' );
const eslint = require( 'gulp-eslint' );
const gulp = require( 'gulp' );
const gutil = require( 'gulp-util' );
const imagemin = require( 'gulp-imagemin' );
const notify = require( 'gulp-notify' );
const plumber = require( 'gulp-plumber' );
const postcss = require( 'gulp-postcss' );
const rename = require( 'gulp-rename' );
const sass = require( 'gulp-sass' );
const sassLint = require( 'gulp-sass-lint' );
const sourcemaps = require( 'gulp-sourcemaps' );
const uglify = require( 'gulp-uglify' );

/**
 * Edit the paths below with your file structure
 */
const paths = {
    css: [ 'library/css/*.css', '!library/css/*.min.css' ],
    images: [ 'library/images/**' ],
    php: [ './*.php', './**/*.php' ],
    sass: 'library/scss/**/*.scss',
    scripts: [ 'library/js/*.js', '!library/js/*.min.js' ],
    sprites: 'library/images/*.png'
};

/**
 * Handle errors and alert the user.
 */
function handleErrors() {
    const args = Array.prototype.slice.call( arguments );

    notify
        .onError( {
            title: 'Task Failed [<%= error.message %>',
            message: 'See console.',
            sound: 'Sosumi' // See: https://github.com/mikaelbr/node-notifier#all-notification-options-with-their-defaults
        } )
        .apply( this, args );

    gutil.beep(); // Beep 'sosumi' again.

    // Prevent the 'watch' task from stopping.
    this.emit( 'end' );
}

/**
 * Delete style.css and style.min.css before we minify and optimize
 */
gulp.task( 'clean:styles', () => del( [ 'library/css/style.css', 'library/css/style.min.css' ] ) );

/**
 * Compile Sass and run stylesheet through PostCSS.
 *
 * https://www.npmjs.com/package/gulp-sass
 * https://www.npmjs.com/package/gulp-postcss
 * https://www.npmjs.com/package/gulp-autoprefixer
 * https://www.npmjs.com/package/css-mqpacker
 */
gulp.task( 'postcss', [ 'clean:styles' ], () =>
    gulp
        .src( 'library/scss/*.scss', paths.css )

        // Deal with errors.
        .pipe( plumber( { errorHandler: handleErrors } ) )

        // Wrap tasks in a sourcemap.
        .pipe( sourcemaps.init() )

        // Compile Sass using LibSass.
        .pipe(
            sass( {
                errLogToConsole: true,
                outputStyle: 'expanded' // Options: nested, expanded, compact, compressed
            } )
        )

        // Parse with PostCSS plugins.
        .pipe(
            postcss( [
                autoprefixer( {
                    browsers: [ 'last 4 versions' ]
                } )
            ] )
        )

        // Create sourcemap.
        .pipe( sourcemaps.write() )

        // Create style.css.
        .pipe( gulp.dest( 'library/css/' ) )
        .pipe( browserSync.stream() )
);

/**
 * Minify and optimize style.css.
 *
 * https://www.npmjs.com/package/gulp-cssnano
 */
gulp.task( 'cssnano', [ 'postcss' ], () =>
    gulp
        .src( 'library/css/style.css' )
        .pipe( plumber( { errorHandler: handleErrors } ) )
        .pipe(
            cssnano( {
                safe: true // Use safe optimizations.
            } )
        )
        .pipe( rename( 'style.min.css' ) )
        .pipe( gulp.dest( 'library/css/' ) )
        .pipe( browserSync.stream() )
);

/**
 * Delete the images before we minify.
 */
gulp.task( 'clean:images', () => del( [ 'dist/library/images/*' ] ) );

/**
 * Optimize images.
 *
 * https://www.npmjs.com/package/gulp-imagemin
 */
gulp.task( 'imagemin', () =>
    gulp
        .src( paths.images )
        .pipe( plumber( { errorHandler: handleErrors } ) )
        .pipe(
            imagemin( {
                optimizationLevel: 5,
                progressive: true,
                interlaced: true,
                svgoPlugins: [
                    {
                        removeViewBox: false,
                        removeAttrs: { attrs: [ 'xmlns' ] }
                    }
                ]
            } )
        )
        .pipe( gulp.dest( 'dist/library/images' ) )
);

/**
 * Concatenate and transform JavaScript.
 * Old way of doing things.
 *
 * https://www.npmjs.com/package/gulp-concat
 * https://github.com/babel/gulp-babel
 * https://www.npmjs.com/package/gulp-sourcemaps
 */
gulp.task( 'concat', () =>
    gulp
        .src( paths.scripts )

        // Deal with errors.
        .pipe( plumber( { errorHandler: handleErrors } ) )

        // Start a sourcemap.
        .pipe( sourcemaps.init() )

        // Convert ES6+ to ES2015.
        .pipe(
            babel( {
                presets: [
                    [
                        'env',
                        {
                            targets: {
                                browsers: [ 'last 4 versions' ]
                            }
                        }
                    ]
                ]
            } )
        )

        // Concatenate partials into a single script.
        .pipe( concat( 'all-tcu-scripts.js' ) )

        // Append the sourcemap to all-tcu-scripts.js.
        .pipe( sourcemaps.write() )

        // Save all-tcu-scripts.js
        .pipe( gulp.dest( 'library/js/concat' ) )
        .pipe( browserSync.stream() )
);

/**
 * Minify compiled JavaScript.
 *
 * https://www.npmjs.com/package/gulp-uglify
 */
gulp.task( 'uglify', () =>
    gulp
        .src( paths.scripts )
        .pipe( plumber( { errorHandler: handleErrors } ) )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe(
            babel( {
                presets: [
                    [
                        'env',
                        {
                            targets: {
                                browsers: [ 'last 4 versions' ]
                            }
                        }
                    ]
                ]
            } )
        )
        .pipe(
            uglify( {
                mangle: false
            } )
        )
        .pipe( gulp.dest( 'library/js/min' ) )
);

/**
 * Sass linting.
 *
 * https://www.npmjs.com/package/sass-lint
 */
gulp.task( 'sass:lint', () =>
    gulp
        .src( [ 'library/scss/**/*.scss', '!library/scss/core/_normalize.scss', '!node_modules/**' ] )
        .pipe( sassLint() )
        .pipe( sassLint.format() )
        .pipe( sassLint.failOnError() )
);

/**
 * JavaScript linting.
 *
 * https://www.npmjs.com/package/gulp-eslint
 */
gulp.task( 'js:lint', () =>
    gulp
        .src( [
            'library/js/*.js',
            'library/js/compiled/*.js',
            '!dist/library/js/**.js',
            '!library/js/min/*.min.js',
            '!Gruntfile.js',
            '!Gulpfile.js',
            '!node_modules/**'
        ] )
        .pipe( eslint() )
        .pipe( eslint.format() )
        .pipe( eslint.failAfterError() )
);

/**
 * Process tasks and reload browsers on file changes.
 *
 * https://www.npmjs.com/package/browser-sync
 */
gulp.task( 'watch', function() {

    // Kick off BrowserSync.
    browserSync( {
        open: false, // Open project in a new tab?
        injectChanges: true, // Auto inject changes instead of full reload.
        proxy: 'localhost/', // Use your localhost path to use BrowserSync.
        watchOptions: {
            debounceDelay: 1000 // Wait 1 second before injecting.
        }
    } );

    gulp.watch( paths.php ).on( 'change', browserSync.reload );
    gulp.watch( paths.sass, [ 'styles' ] );
    gulp.watch( paths.scripts, [ 'scripts' ] );
    gulp.watch( paths.concat_scripts, [ 'scripts' ] );
} );

/**
 * Create individual tasks.
 */
gulp.task( 'styles', [ 'cssnano' ] );
gulp.task( 'sprites', [ 'spritesmith' ] );
gulp.task( 'lint', [ 'sass:lint', 'js:lint' ] );
gulp.task( 'copy', [ 'copy', 'imagemin' ] );
gulp.task( 'default', [ 'styles', 'scripts', 'imagemin' ] );
