**TCU Child Theme Starter Kit**

This is the TCU Child Theme Starter kit to start building WordPress themes for TCU. This starter kit comes bundled with
_[Gulp](https://gulpjs.com/)_, _[LibSass](https://sass-lang.com/)_, _[Babel](https://babeljs.io/)_, _[PostCSS](https://github.com/postcss/postcss)_, _[BrowserSync](https://www.browsersync.io/)_, _[WordPress Coding Standards](https://codex.wordpress.org/WordPress_Coding_Standards)_, and _[PHP Code Sniffer](https://github.com/squizlabs/PHP_CodeSniffer)_.

## Getting Started

To get started you will need basic knowledge of the command line. Go to our _[Getting Started](https://webmanage.tcu.edu/installing-phpcs-eslint-sass-lint/)_ page and follow the directions.

## Prerequisites

* _[Node](https://nodejs.org/en/)_
* _[Gulp CLI](https://github.com/gulpjs/gulp-cli)_

# Important!

Make sure you update file paths in `functions.php`. Update the `text_domain`, and `theme_slug` in `phpcs.xml`. In `Gulpfile.js` you will need to update paths that correspond to your file structure.

# Start your project

Assuming that the `Gulp CLI` has been installed and that the project has already been configured with a package.json and a `Gulpfile.js`, it's very easy to start working with Gulp:

    - Change to the project's root directory.
    - Install project dependencies with `npm install`.
    - Run Gulp with `gulp watch` or `gulp`.
